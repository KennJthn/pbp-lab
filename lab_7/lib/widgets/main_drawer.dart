import 'package:flutter/material.dart';

import '../icons/micon.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, VoidCallback tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Lindungi Peduli',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),

          // Belum dipakai semua
          // TODO: Account
          buildListTile('Login or Create Account', Icons.person, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          
          // TODO: Add Discussion Thread
          buildListTile('Add Discussion', Icons.add, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),

          // TODO: Vaccine
          buildListTile('Vaksin', Micon.syringe, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),

          // TODO: Dictionary
          buildListTile('Kamus Obat', Micon.book_medical, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
        ],
      ),
    );
  }
}