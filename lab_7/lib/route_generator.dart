import 'package:flutter/material.dart';

import './screens/home_page.dart';
import './screens/add_thread.dart';

class RouteGenerator {
    static Route<dynamic> generateRoute(RouteSettings settings) {
        final args = settings.arguments;

        switch (settings.name) {
            case '/':
                return MaterialPageRoute(builder: (_) => HomePage());
            case '/Add-Thread':
                return MaterialPageRoute(builder: (_) => AddThread());
            default:
                return _errorRoute();
        }
    }

    static Route<dynamic> _errorRoute() {
        return MaterialPageRoute(builder: (_) {
            return Scaffold(
                appBar: AppBar(
                    title: Text('Error'),
                    ),
                body: Center(
                    child: Text('ERROR'),
                ),
            );
        });
    }
}