import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class AddThread extends StatefulWidget {
    const AddThread({Key? key}) : super(key: key);

    @override
    State<AddThread> createState() => _AddThreadState();
}

class _AddThreadState extends State<AddThread> {
    final _formKey = GlobalKey<FormState>();
    
    @override
    Widget build(BuildContext context) {
        return Scaffold(
            appBar: AppBar(
                title: Container(
                    child: Image.asset('assets/images/logokotak.png',
                    height: 56.0),
                ),
            ),
            drawer: MainDrawer(),
            body: Form(
                key: _formKey,
                child: Column(
                    children: <Widget>[
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                                decoration: const InputDecoration(
                                    hintText: 'What are you writing about?',
                                    labelText: 'Topic',
                                ),
                                onSaved: (String? value) {
                                    print("Apa tu man");
                                },
                                validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Are you even trying? o_o';
                                    }
                                    return null;
                                },
                            ),
                        ),
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: TextFormField(
                                decoration: const InputDecoration(
                                    hintText: 'Brief explanation of your problem',
                                    labelText: 'Argument',
                                ),
                                onSaved: (String? value) {
                                    print("Apa tu man");
                                },
                                validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                        return 'Are you even trying? o_o';
                                    }
                                    return null;
                                },
                            ),
                        ),
                        Padding (
                            padding: const EdgeInsets.all(5.0),
                            child: ElevatedButton(
                                onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                        print('valid');
                                        ScaffoldMessenger.of(context).showSnackBar(
                                            const SnackBar(content: Text('Processing Data')),
                                        );
                                    } else {
                                        print('not valid');
                                    }
                                },
                                child: const Text('Submit'),
                            ),
                        ),
                    ],
                ),
            ),
            floatingActionButton: FloatingActionButton(
                onPressed: () {
                    Navigator.of(context).pushNamed(
                        '/',
                    );
                },
                tooltip: 'Go back',
                child: const Icon(Icons.add),
            ),
        );
    }
}
