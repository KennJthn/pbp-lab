Apakah perbedaan antara JSON dan XML?
* JSON (JavaScript Object Notation):
1. Bahasanya merupakan turunan dari JavaScript
2. Merepresentasikan object dalam bentuk list
3. Object JSON memiliki type
4. Hanya bisa menggunakan encoding UTF-8
5. Tidak menggunakan tag
6. Secara umum, lebih ringkas karena tidak memakai tag pembuka dan penutup
7. Bisa memakai array
8. Lebih cepat untuk read dan write
9. JSON bisa di-parse menggunakan JavaScript function

* XML (Extensible Markup Language):
1. Bahasanya merupakan turunan dari SGML
2. Merepresentasikan data dalam bentuk struktur tag
3. data XML tidak memiliki type (typeless)
4. Bisa menggunakan berbagai encoding
5. Menggunakan tag
6. Secara umum, lebih panjang karena memakai tag pembuka dan penutup
7. Tidak bisa memakai array
8. Lebih lambat untuk read dan write
9. XML harus menggunakan XML parser

Sumber:
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.w3schools.com/js/js_json_xml.asp
https://www.guru99.com/json-vs-xml-difference.html

Apakah perbedaan antara HTML dan XML?
* HTML:
1. Digunakan untuk menampilkan data
2. Tidak case-sensitive
3. Memilki tag yang predefined
4. Statis
5. White spaces dibuang
6. Parsing tidak memerlukan kode atau aplikasi tambahan
7. HTML terkadang tidak memerlukan closing tag

* XML:
1. Digunakan untuk mengirimkan data
2. Case-sensitive
3. Tidak memiliki tag yang predefined. Tags di-define sesuai kebutuhan programmer sehingga lebih fleksibel
4. Dinamis
5. White spaces dipertahankan
6. Parsing memerlukan XML parser (XML DOM) dan kode tambahan
7. XML selalu menggunakan closing tag

Sumber:
https://www.geeksforgeeks.org/html-vs-xml/
https://www.upgrad.com/blog/html-vs-xml/
https://www.guru99.com/xml-vs-html-difference.html