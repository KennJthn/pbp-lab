from django.db import models

# Create your models here.
class Note(models.Model):
    to      = models.CharField(max_length=30)
    From    = models.CharField(max_length=30) # "from" di ban T-T
    title   = models.CharField(max_length=1000)
    message = models.TextField(max_length=1000)