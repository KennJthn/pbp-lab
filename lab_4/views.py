from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    note = Note.objects.all().values()
    
    response = {'note': note}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    context = {}

    form = NoteForm(request.POST or None)

    if form.is_valid():
        form.save()

    context['friendForm'] = form
    return render(request, 'lab4_form.html', context)

@login_required(login_url='/admin/login/')
def note_list(request): # CARD
    note = Note.objects.all().values()

    response = {'note': note}
    return render(request, 'lab4_note_list.html', response)