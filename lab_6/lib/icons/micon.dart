import 'package:flutter/widgets.dart';

class MyIcons {
    MyIcons._();

    static const _kFontFam = 'MyIcons';
    static const String _kFontPkg = 'MyPackage';

    static const IconData suntik =
      IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
    static const IconData kamus =
      IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}