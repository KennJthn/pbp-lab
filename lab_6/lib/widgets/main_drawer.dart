import 'package:flutter/material.dart';

import '../screens/filters_screen.dart';
import '../icons/micon.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  Widget tambahSuntikTile(BuildContext context) {
    return ListTile(
      leading: Icon(
        MyIcons.suntik,
        size: 26,
      ),
      title: Text(
        'Vaksin',
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        Navigator.of(context).pushReplacementNamed('/');
      },
    );
  }

  Widget tambahKamusTile(BuildContext context) {
    return ListTile(
      leading: Icon(
        MyIcons.kamus,
        size: 26,
      ),
      title: Text(
        'Kamus Obat',
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {
        Navigator.of(context).pushReplacementNamed('/');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Lindungi Peduli',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),

          // Belum dipakai semua
          // TODO: Account
          buildListTile('Login or Create Account', Icons.person, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          
          // TODO: Add Discussion Thread
          buildListTile('Add Discussion', Icons.add, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),

          // TODO: Vaccine
          tambahSuntikTile(context),

          // TODO: Dictionary
          tambahKamusTile(context),
        ],
      ),
    );
  }
}
